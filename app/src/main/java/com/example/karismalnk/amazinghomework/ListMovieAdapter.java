package com.example.karismalnk.amazinghomework;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class ListMovieAdapter extends BaseAdapter{
    Context context;
    ArrayList<String> listMovieName = new ArrayList<>();
    ArrayList<Integer> listMovieTitle = new ArrayList<>();

    public ListMovieAdapter(Context context) {
        this.context = context;
        getData();
    }

    private void getData() {
        // movie title
        listMovieName.add("avatar");
        listMovieName.add("gladiator");
        listMovieName.add("GSM");
        listMovieName.add("Spiderman");
        listMovieName.add("Star Wars");
        listMovieName.add("The Hobbit");

        // movie cover filename
        listMovieTitle.add(R.drawable.avatar);
        listMovieTitle.add(R.drawable.gladiator);
        listMovieTitle.add(R.drawable.gs);
        listMovieTitle.add(R.drawable.homecoming);
        listMovieTitle.add(R.drawable.last_jedi);
        listMovieTitle.add(R.drawable.the_hobbit);
    }

    // must be provided. otherwise, the adapter will not load any data
    @Override
    public int getCount() {
        return listMovieName.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
//        TextView dummyTextView = new TextView(context);
//        dummyTextView.setText(listMovieName.get(position));
//        return dummyTextView;

        if (convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.movie_item, null);
        }

        final ImageView imageView = (ImageView)convertView.findViewById(R.id.movie_cover_art);
        final TextView titleTextView = (TextView)convertView.findViewById(R.id.movie_title);

        imageView.setImageResource(listMovieTitle.get(position));
        titleTextView.setText(listMovieName.get(position));

        return convertView;
    }
}
