package com.example.karismalnk.amazinghomework;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button btnMovie;
    private Button btnTheater;
    public static FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnMovie = (Button) findViewById(R.id.btnMovie);
        btnTheater = (Button) findViewById(R.id.btnTheater);
    }

    @Override
    protected void onResume() {
        super.onResume();

        // start Fragment Movie List on content component
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.content, new FragmentMovieList()).commit();
    }

    public void ButtonMovie(View v) {
        // open Multi activity
        Intent intent = new Intent(MainActivity.this, MultiActivity.class);
        intent.putExtra("fragmentType", 1); // start Fragment Movie
        startActivity(intent);
    }

    public void ButtonTheater(View v) {
        // open Multi activity
        Intent intent = new Intent(MainActivity.this, MultiActivity.class);
        intent.putExtra("fragmentType", 2); // start Fragment Theater
        startActivity(intent);
    }

}
