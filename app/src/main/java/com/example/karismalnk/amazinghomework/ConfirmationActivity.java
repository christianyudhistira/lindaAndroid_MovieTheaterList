package com.example.karismalnk.amazinghomework;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ConfirmationActivity extends AppCompatActivity {

    private TextView movieTitle;
    private TextView seatNumber;
    private Button confirmButton;

    private String seatNumberDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);

        movieTitle = (TextView) findViewById(R.id.movieTitle);
        seatNumber = (TextView) findViewById(R.id.seatNumber);
        confirmButton = (Button) findViewById(R.id.confirmButton);
    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = getIntent();
        String[] seatNumberList = intent.getStringArrayExtra("nomorbangku");
        seatNumberDisplay = seatNumberList[0];

        movieTitle.setText(intent.getStringExtra("judulfilm"));
        seatNumber.setText(seatNumberDisplay);
    }

    public void confirmButton(View v) {
        Intent intent = new Intent(ConfirmationActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
