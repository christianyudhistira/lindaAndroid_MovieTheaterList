package com.example.karismalnk.amazinghomework;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by christian yudhistira on 02/04/2018.
 */

public class FragmentSeat extends Fragment {
    private ArrayList<String> seatOrderNumber = new ArrayList<>();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_seat, container, false);

        // point to Layout component
        GridView seatList = (GridView) view.findViewById(R.id.listSeat);
        final TextView movieTitle = (TextView) view.findViewById(R.id.movieTitle);
        Button processBtn = (Button) view.findViewById(R.id.processButton);

        // set movie title
        Bundle data = getArguments();
        final String title = data.getString("movietitle");
        movieTitle.setText(title);

        // create adapter
        final SeatAdapter seatAdapter = new SeatAdapter(getContext());

        // set Movie content
        seatList.setAdapter(seatAdapter);

        // onClick handler for each Seat list
        seatList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                seatOrderNumber.add(seatAdapter.listSeatNumber.get(position));
            }
        });

        // onClick handler for Process Button
        processBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                movieTitle.setText(String.valueOf(seatOrderNumber.size()));
                String[] numSeat = new String[seatOrderNumber.size()];
                for (int x=0; x<seatOrderNumber.size(); x++) {
                    numSeat[x] = new String(seatOrderNumber.get(x));
                }

                Intent intent = new Intent(getContext(), ConfirmationActivity.class);
                intent.putExtra("judulfilm", title);
                intent.putExtra("nomorbangku", numSeat);
                startActivity(intent);

                getActivity().finish();
            }
        });

        return view;
    }
}
