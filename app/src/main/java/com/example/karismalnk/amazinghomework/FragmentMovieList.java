package com.example.karismalnk.amazinghomework;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

public class FragmentMovieList extends Fragment {
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_list, container, false);

        // point to Layout component
        GridView movieList = (GridView) view.findViewById(R.id.listMovie);

        // create adapter
        final ListMovieAdapter listMovieAdapter = new ListMovieAdapter(getContext());

        // set Movie content
        movieList.setAdapter(listMovieAdapter);

        // onClick handler for each Movie list
        movieList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                // open Multi activity
                Intent intent = new Intent(getContext(), MultiActivity.class);
                intent.putExtra("judulfilm", listMovieAdapter.listMovieName.get(position)); // passing Movie Title
                intent.putExtra("fragmentType", 3); // start Fragment Seat
                startActivity(intent);
            }
        });

        return view;
    }
}
