package com.example.karismalnk.amazinghomework;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MultiActivity extends AppCompatActivity {
    public static FragmentManager fragmentManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi);

        // start Fragment Movie List on contentMovie component
        fragmentManager = getSupportFragmentManager();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent intent = getIntent();
        int fragType = intent.getIntExtra("fragmentType", 0);

        // data container between fragment
        Bundle data = new Bundle();

        if (fragType == 1) { // start movie fragment
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.contents, new FragmentMovieList()).commit();
        }
        else if (fragType == 2){
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.contents, new FragmentTheater()).commit();
        }
        else if (fragType == 3) { // start seat fragment
            data.putString("movietitle", intent.getStringExtra("judulfilm")); // receive Movie Title from Movie fragment and pass it to Seat fragment

            Fragment fragment = new FragmentSeat();
            fragment.setArguments(data);
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.replace(R.id.contents, fragment).commit();
        }
    }
}
