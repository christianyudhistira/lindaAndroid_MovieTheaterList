package com.example.karismalnk.amazinghomework;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by christian yudhistira on 03/04/2018.
 */

public class SeatAdapter extends BaseAdapter {
    Context context;

    ArrayList<String> listSeatNumber = new ArrayList<>();

    public SeatAdapter(Context c) {
        this.context = c;
        getData();
    }

    private void getData() {
        listSeatNumber.add("A1");
        listSeatNumber.add("A2");
        listSeatNumber.add("A3");
        listSeatNumber.add("A4");
        listSeatNumber.add("A5");
        listSeatNumber.add("A6");

        listSeatNumber.add("B1");
        listSeatNumber.add("B2");
        listSeatNumber.add("B3");
        listSeatNumber.add("B4");
        listSeatNumber.add("B5");
        listSeatNumber.add("B6");

        listSeatNumber.add("C1");
        listSeatNumber.add("C2");
        listSeatNumber.add("C3");
        listSeatNumber.add("C4");
        listSeatNumber.add("C5");
        listSeatNumber.add("C6");

        listSeatNumber.add("D1");
        listSeatNumber.add("D2");
        listSeatNumber.add("D3");
        listSeatNumber.add("D4");
        listSeatNumber.add("D5");
        listSeatNumber.add("D6");

        listSeatNumber.add("E1");
        listSeatNumber.add("E2");
        listSeatNumber.add("E3");
        listSeatNumber.add("E4");
        listSeatNumber.add("E5");
        listSeatNumber.add("E6");

        listSeatNumber.add("F1");
        listSeatNumber.add("F2");
        listSeatNumber.add("F3");
        listSeatNumber.add("F4");
        listSeatNumber.add("F5");
        listSeatNumber.add("F6");
    }

    @Override
    public int getCount() {
        return listSeatNumber.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
//        TextView dummyTextView = new TextView(context);
//        dummyTextView.setText(String.valueOf(position));
//        return dummyTextView;

        if (convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(context);
            convertView = layoutInflater.inflate(R.layout.seat_item, null);
        }

        final TextView titleTextView = (TextView)convertView.findViewById(R.id.seat_number);
        titleTextView.setText(listSeatNumber.get(position));

        return convertView;

    }
}
